# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ''
  str.each_char do |ch|
    new_str << ch unless ('a'..'z').include?(ch)
  end
  new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  length = str.length
  if length.odd?
    return str[(length - 1) / 2]
  else
    return str[(length - 2) / 2, 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count('aeiou')
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (2..num).each do |i|
    product *= i
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ''
  arr.each_with_index do |i, idx|
    new_str << i.to_s
    new_str << separator unless idx == arr.length - 1
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  chars = str.chars
  chars.each_with_index do |i, idx|
    if (idx + 1).odd?
      i.downcase!
    else
      i.upcase!
    end
  end
  chars.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words.each do |i|
    if i.length >= 5
      i.reverse!
    end
  end
  words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 != 0
      arr << 'fizz'
    elsif num % 5 == 0 && num % 3 != 0
      arr << 'buzz'
    elsif num % 3 == 0 && num % 5 == 0
      arr << 'fizzbuzz'
    else
      arr << num
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  until arr.empty?
    new_arr << arr.pop
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1

  (2..num / 2).each do |i|
    if num % i == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |i|
    if num % i == 0
      factors << i
    end
  end
  factors.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  prime_factors = []
  factors.each do |i|
    if prime?(i)
      prime_factors << i
    end
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = arr.select {|i| i.odd?}.length
  evens = arr.select {|i2| i2.even?}.length

  if odds == 1
    return arr.select {|i| i.odd?}[0]
  elsif evens == 1
    return arr.select {|i| i.even?}[0]
  else
    "There is no oddball!"
  end

end
